// @author: Marcos Lima

import {data} from '../fixtures/temp/temp.json'

describe('Template Validation PCs  - Base', function() {

	// for(var i = 0; i < 1; i++) {
	for(var i = 0; i < data.length; i++) {
		const array = data[i];
		checking(array)
    }

	beforeEach(() => {
			cy.visit('/')
			cy.toJson('./cypress/fixtures/templates/mov/vitta.csv')
	})
	
	function checking(array) {
		var message = array.nome_beneficiario
        it('Benficiário: '+message, function () {
			expect(array.plano, 'plano').to.be.not.null
			expect(array.plano, 'plano').to.include('-')
				.and.to.include(' ')
			expect(array.plano.length, 'plano')
				.to.be.greaterThan(10)

			expect(array.codigo_beneficiario, 'cód. beneficiário').to.be.not.null
			expect(array.codigo_beneficiario, 'cód. beneficiário').and.to.include('.')
				.and.to.include('-')
			expect(array.codigo_beneficiario.length, 'cód. beneficiário')
				.to.be.greaterThan(19)
			
			expect(array.cpf, 'CPF').to.be.not.null
				.and.not.to.include('.')
				.and.not.to.include('-')
				.and.not.to.include(' ')
			expect(11, 'CPF')
			  	.to.be.equal(array.cpf.length)

			expect(array.nome_beneficiario, 'nome beneficiário').to.be.not.null
				.and.not.to.match(/[.!#$%&'*+/=?^_`{|}~-]/)
			expect(array.nome_beneficiario.length, 'nome beneficiário')
			  	.to.be.greaterThan(2)
			
			var matricula = parseInt(array.matricula)
			if ((array.tipo == 'TITULAR' && matricula != '') || (array.tipo == 'DEPENDENTE' && matricula != '')) {
				expect(array.matricula.length, 'matricula').to.be.greaterThan(2)
					.and.to.include(/[0-9]/)
			}else if (array.tipo == 'DEPENDENTE' && matricula == '') {
				expect(array.matricula.length, 'matricula').to.be.not.null
					.and.to.include(/[0-9]/)
			} else {
				cy.log('matricula: is empty')
			}

			if (array.tipo, 'tipo' != '') {
				expect(array.tipo, 'tipo').to.match(/TITULAR|DEPENDENTE/)
			} else {
				cy.console('tipo: is empty')
			}
			
			if (array.sexo, 'sexo' != '') {
				expect(array.sexo.length, 'sexo').to.be.equal(1)
				expect(array.sexo, 'sexo').to.match(/M|m|F|f/)
			} else {
				cy.console('sexo: is empty')
			}

			if (array.data_nascimento != '') {
				cy.checkDate('data de nascimento', array.data_nascimento)
			} else {
				cy.console('data de nascimento: is empty')
			}

			if(array.data_inclusao != '') {
				cy.checkDate('data de inclusao', array.data_inclusao)
			} else {
				cy.console('data de inclusao: is empty')
			}

			const age = parseInt(array.idade)
			if(array.tipo == 'TITULAR') {
				expect(age, 'idade').to.match(/[0-9]/)
				expect(age, 'idade titular').to.be.greaterThan(18)
			} else if(array.tipo == 'DEPENDENTE') {
				expect(age, 'idade').to.match(/[0-9]/)
			} else {
				console.log('Idade or Tipo is null')
			}

			expect(array.situacao, 'situacao').to.match(/ATIVO|INATIVO/)

			if(array.cargo != '') {
				expect(array.cargo.length, 'cargo').to.be.greaterThan(4)
			} else {
				cy.log('data fim inatividade: is empty')
			}

			if(array.centro_de_custo != '') {
				expect(array.centro_de_custo.length, 'centro de custo').to.be.greaterThan(2)
			} else {
				cy.log('data fim inatividade: is empty')
			}

			if(array.data_fim_inatividade != '') {
				cy.checkDate('data fim inatividade', array.data_fim_inatividade)
				cy.compareDate(array.data_inclusao, array.data_fim_inatividade)
			} else {
				cy.log('data fim inatividade: is empty')
			}

			expect(array.email, 'email')
			  	.to.match(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/)
			
			expect(array.telefone.length, 'telefone')
			  	.to.be.within(10,17)
			expect(array.telefone, 'telefone')
			  	.to.match(/^(?:(?:\+|00)?(55)\s?)?(?:\(?([1-9][0-9])\)?\s?)(?:((?:9\d|[2-9])\d{3})\-?(\d{4}))$/gm)
		})
    }
})