// @author: Marcos Lima

import {data} from '../../fixtures/temp/temp.json'

describe('Template Validation PCs  - Base', function() {

    let holders = []
    let dependants = []

	for(var i = 0; i < data.length; i++) {
        if(data[i].tipo == 'TITULAR') {
            holders.push(data[i])
        } else if (data[i].tipo == 'DEPENDENTE') {
            dependants.push(data[i])
        }
    }

    for(var i = 0; i < holders.length; i++) {
        for(var j = 0; j < dependants.length; j++) {
            const holder = holders[i];
            const dependant = dependants[j];
            checking(holder, dependant)
        }
    }

	beforeEach(() => {
		cy.visit('/')
	})
	
	function checking(holder, dependant) {
        var message = holder.nome_beneficiario
        it('Dependentes titular: '+message, function () {
			if (dependant.matricula == holder.matricula) {
				cy.log(dependant.nome_beneficiario+' é dependente de '+holder.nome_beneficiario+'.')
			} else if (dependant.matricula != holder.matricula) {
                throw new Error(dependant.nome_beneficiario+' não é dependente do titular '+holder.nome_beneficiario+'.')
            } else if ((dependant.matricula == '') || (holder.matricula == '')) {
                throw new Error('Matricula não pode ser vazia.')
            } else {
				throw new Error('Não foi encontrado vínculo para a vida: ', dependant.nome_beneficiario+'.')
			}
		})
    }
})