// ***********************************************
// https://on.cypress.io/custom-commands
// ***********************************************
Cypress.Commands.add('console', {
    prevSubject: true
    }, (subject, method) => {
    method = method || 'log'
    console[method]('The subject is', subject)
    return subject
})

Cypress.Commands.add('checkDate', (label, string) => {
    if(!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(string))
        return false;
        
    var parts = string.split("/");
    var day = parseInt(parts[0], 10)
    var month = parseInt(parts[1],10)
    var year = parseInt(parts[2],10)

    expect(string, label).to.include('/')
    // expect(string.length, label).to.be.equal(10)
    if(string.length != 10) {
        throw new Error(label+': Invalid format date ('+string+'), expect to: "DD/MM/YYYY"')
    }
    expect(day, label+'(dia)').to.be.within(1,31)
    expect(month, label+'(mês)').to.be.within(1,12)
    expect(year, label+'(ano)').to.be.within(1000,3000)

    if(day <= 12 && month > 12) {
        throw new Error('Invalid format date, Change format date to: "DD/MM/YYYY"')
    }
})

Cypress.Commands.add('compareDate', (date1, date2) => {    
    if(date2 < date1) {
        throw new Error('inativeDate cannot be less than includeDate!'+'\nData inclusao: '+date1+'\nData inativacao: '+date2)
    }
})

//Convert csv to Json
Cypress.Commands.add('toJson', (input, result) => {
    cy.readFile(input).then((csv) => {
        let lines = csv.split("\n");
        let result = [];
        let headers = lines[0].split(",");
        let path = "./cypress/fixtures/temp/temp.json"

        lines[0]= lines[0].toLowerCase()
        
        for(let i=1;i<lines.length;i++) {
            let obj = {};
            let currentline=lines[i].split(",");

            for(let j=0;j<headers.length;j++) {
                obj[headers[j]] = currentline[j];
            }
            result.push(obj);
        }
        console.log('csv original:', lines)
        console.log('JSON convertido:', result)
        cy.writeFile('./cypress/fixtures/temp/temp.json', (result))
        cy.log('JSON armazenado no arquivo: '+path)
    })
    console.log(result)
    return result
})

//Convert Json to .csv
Cypress.Commands.add('toCSV', (input) => {
    cy.readFile(input).then((json) => {
        let array = typeof json != 'object' ? JSON.parse(json) : json;
        let str = '';
        //let headers = array[0].split(":");

        console.log('array: ', array)
        for(let i = 0; i < array.length; i++) {
            let line = '';
            
            for(let index in array[i]) {
                if(line != '') line += ','
                    line += array[i][index];
           
                    //console.log('headers', headers)

                    console.log('line['+i+']:', line)
                    console.log('array['+i+']:', array[i])
            }
            str += line + '\r\n';
        }
        cy.writeFile('./cypress/fixtures/temp/temp.csv', (str))
        cy.log('JSON armazenado no arquivo: ./cypress/fixtures/temp/temp.csv')
        console.log(str)
        //return str;

    })  
})

//Clear temp files
Cypress.Commands.add('clearTemp', () => {
        cy.writeFile('./cypress/fixtures/temp/temp.csv', '')
        cy.writeFile('./cypress/fixtures/temp/temp.json', '')
})