# [![vitta-logo](https://corretoras.vitta.com.br/img/logo-vitta-new.48e093e7.svg)](#) tadashi

[Cypress Documentation](https://on.cypress.io) | [Portal da Corretora](https://corretoras.vitta.com.br/companies)

Este projeto visa validar o layout de PCs, informando quais foram os erros de preenchimento.

## Installing

[![npm version](https://badge.fury.io/js/cypress.svg)](https://badge.fury.io/js/cypress)
[![Cypress.io tests](https://img.shields.io/badge/cypress.io-tests-green.svg?style=flat-square)](https://cypress.io)

Instale o Cypress no Mac, Linux, ou Windows, para isso execute no terminal o comando abaixo ou acesse a [documentação](https://docs.cypress.io/guides/getting-started/installing-cypress.html).

```bash
npm install cypress --save-dev
```

Para executar o projeto pelo terminal, acesse o diretório do projeto e insira o comando:

```bash
npx cypress open
```

Para executar em modo headless:
```bash
npx cypress run
```

![installing-cli e1693232](https://user-images.githubusercontent.com/1271364/31740846-7bf607f0-b420-11e7-855f-41c996040d31.gif)

## Commands

É possível criar comandos personalizados para seus scritps, o Cypress.io redesigna o caminho [./cypress/support/commands.js](https://github.com/markvlima/goatme-solving/blob/master/cypress/support/commands.js) para criação desses comandos.

Atualmente você encontrará as funções abaixo:

* toJson - converte um arquivo csv em um JSON para que possa ser manipulado, basta passar como parâmetro o caminho do arquivo csv.
```
cy.toJson(input)
cy.toJson('./cypress/fixtures/exemplo.csv')
```

* toCSV - converte um arquivo csv em um JSON para que possa ser manipulado, basta passar como parâmetro o caminho do arquivo JSON.
```
cy.toCSV(input)
cy.toCSV('./cypress/fixtures/exemplo.json')
```

* clearTemp - Limpa o conteúdo de todos os arquivos temporários e não necessita de parâmetros.
```
cy.clearTemp()
```

## Project Structure

:file_folder: project
```text
├── cypress
│   ├── fixtures
│   │   ├── docs
│   │   ├── temp
│   │   └── templates
│   │       ├── health
│   │       └── odonto
│   ├── integration
│   │   ├── templates
│   │   └── utils
│   ├── plugins
│   ├── screenshots
│   ├── support
│   └── videos
├── node_modules
├── .gitignore
├── cypress.json
├── package-lock.json
├── package.json
└── README.md
```